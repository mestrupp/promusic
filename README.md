## ProMusic ##

MPCS 51037: Advanced Java
Professor Gerber
August 12th, 2014

### Repository Purpose ###

This repository represents my submission for the proMusic assignment for class.

### Introduction ###

This project is an adaptation of the "pdf" download application discussed in class.  With the professor's permission, I decided to take a slightly different approach to the idea of searching for and downloading files online.  Specifically, I introduced an interim step whereby the user could decide whether to download a file after the search is completed, but without having to download all of the files *en masse*. 

This JavaFX application, called JavaFX Image Retriever, is designed to assist in the identification and retrieval of image (.png) files from three designated online image repositories: flickr.com, bigstock.com and openclipart.org.  Users enter a search term and receive a set of thumbnail images to review. Only when the user identifies and selects an image he or she likes, then that image file can be downloaded and saved in a designated directory on their computer.  However, if the user wants, they can also download all of the identified files all at once.

### How To Use The Software ###

I designed this software to be very easy to use.  Once the program launches, the user is presented with a screen similar to the one below:

![IR-screenshot-1.png](https://bitbucket.org/repo/yAE7p6/images/2179837438-IR-screenshot-1.png)

The user enters in one or more search terms and checks which of the three image repositories he or she wishes to search.  The user then presses the "Search" button or hits the ENTER key and the search commences.  In a few moments, the tile pane in the middle of the screen will begin to fill with thumbnail images.  This is done using a multi-threaded approach.

![IR-screenshot-2.png](https://bitbucket.org/repo/yAE7p6/images/2813372869-IR-screenshot-2.png)

Once the initial search is complete, the user can choose to:

1. Click on the "Load Page 2" button which will load the next page of search results from each repository (this increments with each new page loaded);
2. Type in a new search term and hit the "Search" button, which will commence a new search; or
3. Click on one of the image thumbnails, which will begin the downloading process of the actual image file (see below).

![IR-screenshot-3.png](https://bitbucket.org/repo/yAE7p6/images/1908846321-IR-screenshot-3.png)

Files are saved in a sub-directory that is named after the search term used for the search.  For example, if our root directory is "c:\images\" and the user conducts a search using the "baby" keyword, the images would be saved in a directory called "c:\images\baby\".  File names follow the format the source-name_source-id.png.  For example, the image file for Flickr image no. 482930123 would be called "flickr_482930123.png"

The user also has the option of downloading all of the current images displayed in the middle tile pane. This is done by clicking on the "Download All" button that appears at the bottom of the page.  Once this button is clicked, all of the images are downloaded to the appropriate folder:

![IR-screenshot-4.png](https://bitbucket.org/repo/yAE7p6/images/1074114459-IR-screenshot-4.png)

Once a file has downloaded, the user can decide whether to (a) delete the image file, (b) View the image file or (c) Open the folder in which the image file was stored, using buttons that appear beside each download towards the bottom of the screen window.  

It is expected that once the user has downloaded enough files, he or she will open up another application (for example, an image manipulation program) to work with these images.

**Special Note About BigStock.com Images: ** BigStock.com is a commercial stock photography service. Therefore, they only allow for the free downloading of thumbnail images. Full images must be paid for. Consequently, for this demonstration, this program downloads the thumbnail image.  For the other repositories, the full size image is retrieved.

### Dependencies ###

In order to create JavaFX dialog boxes, I incorporated the free controlfx classes.  You can read more about this project on their [bitbucket home page](http://controlsfx.bitbucket.org/overview-summary.html).  Consequently, you should first "Build With Dependencies" in Netbeans before attempting to run this project.