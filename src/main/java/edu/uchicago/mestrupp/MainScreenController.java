
package edu.uchicago.mestrupp;

import com.google.gson.Gson;
import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.DirectoryChooser;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;

public class MainScreenController implements Initializable {

    @FXML
    private Button btnSearch;

    @FXML
    private Pane pneUpper;

    @FXML
    private TextField txtSearchTerm;

    @FXML
    private Label lblPrompt;

    @FXML
    private BorderPane brdPane;
    
    @FXML
    private CheckBox chkBigStock;

    @FXML
    private CheckBox chkFlickr;
    
    @FXML
    private CheckBox chkClipArt;
    
    @FXML
    private ScrollPane scrollMiddle;
    
    @FXML
    private TilePane tileMiddle;
    
    @FXML
    private Button btnSelectDir;
    
    @FXML
    private Label lblSelectDir;
    
    @FXML
    private AnchorPane anchorLower;
    
    @FXML
    private TextField txtSelectDir;

    @FXML
    private Button btnClearDownloads;
    
    @FXML
    private VBox vbox;
    
    @FXML
    private ScrollPane scrollBottom;

    @FXML
    private Button btnDownloadAll;
    
    // We default to a root directory called "\images\" although
    // users can choose another directory by clicking on the btnSelectDir button
    
    protected String fileDirectory = "\\images\\";
    
    // The following two variables are used for tracking when the user wants
    // to continue onto the next "page" of a search or wants instead to start a new search
    
    Boolean newSearch;
    int pageCount;
    
    // These controls are used each time we download a file to the local computer.
    // We identify them here so that we can instantiate new versions of them
    // within sub-threads without running into threading errors.
    
    HBox newHBox;
    ProgressBar newProgBar;
    Label lblDescription;
    Button btnCancel;
    Button btnShow;
    Button btnFolder;
        
    // The initialize method below is used to set various parameters and methods
    // for important controls within our scene.
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {

         vbox.setPadding(new Insets(15));
         
         // This adjustment ensures that our bottom scroll pane properly
         // scrolls down when a new file is downloaded.
         
         vbox.heightProperty().addListener(new ChangeListener() {
             @Override
            public void changed(ObservableValue observable, Object oldvalue, Object newValue) {
                scrollBottom.setVvalue((Double)newValue );  
            }
         });
         
         // This ensures that our tilePane in the middle correctly adjusts
         // when the window is re-sized
         
         scrollMiddle.setFitToWidth(true);
         scrollMiddle.setFitToHeight(true);
         
         // We do the same for our bottom scroll bar.
         scrollBottom.setFitToWidth(true);
         
         // We set the padding and margins for our middle tilePane
         tileMiddle.setPadding(new Insets(10));
         tileMiddle.setHgap(10);
         tileMiddle.setVgap(10);
    
         // Set the newSearch flag to false and page count to zero (default values)
         newSearch = false;
         pageCount = 0;
 
         // If the user has selected the txtSearchTerm text field and hits
         // the enter key, this initiates the search function. Also, if they enter a key
         // in this text field that is not the Enter key, the program will assume they want to do a new search.
         
         txtSearchTerm.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
             public void handle(KeyEvent ke) {
              if (ke.getCode().equals(KeyCode.ENTER)) {
                  try {
                      startSearch(new ActionEvent());
                  } catch (UnsupportedEncodingException ex) {
                       Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
                  } catch (InterruptedException ex) {
                       Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
                  }
               } else {
                    btnSearch.setText("Search");
                    newSearch = true;
               }
              }
            });
      
            // We set the textSearchTerm text field to be not traversable so that the text field
            // is not selected by accident, which would result in resetting a search.
         
            txtSearchTerm.setFocusTraversable(false);
            
            // We also set a helpful tool tip for the change directory button
            btnSelectDir.setTooltip(new Tooltip("Click To Select Directory"));
         
    }
   
    @FXML
    private void startSearch(javafx.event.ActionEvent event) throws UnsupportedEncodingException, InterruptedException {
        
        // We retrieve the search term from the text field, trim it and remove any extra spaces too
        final String searchTerm = txtSearchTerm.getText().trim().replaceAll("\\s+", " "); 

        // We then evaluate whether there is actually a search term entered and flag if it is not.        
        
        // The following two error messages incorporate the controlsfx JavaFX dialog control
        // More information about this can be found at http://controlsfx.bitbucket.org/
        
        if(searchTerm.equals("")){
            Action response = Dialogs.create()
                    .title("No Search Term Entered")
                    .masthead("Unfortunately, we are unable to process your request")
                    .message("Please enter a term (or terms) to search for, and try again.")
                    .showError();
            txtSearchTerm.requestFocus();
            return;            
        }
        
        // We check to see also whether any source checkboxes are selected
        
        if(chkFlickr.isSelected() == false && chkBigStock.isSelected() == false && chkClipArt.isSelected() == false){
            Action response = Dialogs.create()
                    .title("No Websites Selected")
                    .masthead("Unfortunately, we are unable to process your request")
                    .message("Please select one or more websites to search by using the checkboxes located to the right of the search button.")
                    .showError();
            txtSearchTerm.requestFocus();
            return;
        }
        
        // Finally, we make sure there are no funky characters in the search term
        // Unusual characters would be problematic as we use the search term to create our sub-folders in which we 
        // store downloaded images.
        
        if(!Pattern.matches("[a-zA-Z ]+",txtSearchTerm.getText().trim())){
            Action response = Dialogs.create()
                    .title("Illegal Characters In Search Term")
                    .masthead("Unfortunately, we are unable to process your request")
                    .message("Valid search terms may only contain letters and spaces. No numbers or symbols are allowed.")
                    .showError();
            txtSearchTerm.requestFocus();
            return;
        }

        if(newSearch) { 
            pageCount = 0;
        } 
                 
        tileMiddle.getChildren().clear();
        pageCount = pageCount + 1;
        
        final String encodeSearch = URLEncoder.encode(txtSearchTerm.getText().trim(),"UTF-8");
        final ArrayList<ImageResult> resultList = new ArrayList<>();
                
        btnSearch.setText("Searching ...");
        btnSearch.setDisable(true);
        
        // This is the executor that we'll use to download search results from each of the three
        // repositories.
        
        ThreadPoolExecutor executor = (ThreadPoolExecutor)Executors.newFixedThreadPool(3);
         
        // <editor-fold defaultstate="collapsed" desc="Download Thumbnail Code">
        
        // The way this code works is that we first create three tasks (one for each source type)
        // This is necessary as each source type has its own quirks in terms of what type of JSON data
        // they send back. For example, flickr wraps their JSON with another label which we have to remove.
        
        // We then examine which checkboxes are checked and initiate the appropriate tasks to retrieve the search results
        // and display thumbnail images.
        
        final Task<ArrayList<ImageResult>> flickrTask  = new Task<ArrayList<ImageResult>>() {

            ArrayList<ImageResult> returnList = new ArrayList<ImageResult>();
            
            @Override
            protected ArrayList<ImageResult> call() throws InterruptedException {
                
                updateMessage("Flickr.com -- Checking ...");

                String flickrRequest = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=c49a2aa8ca2e9e1440256675cacbcdc6&tags=" + encodeSearch + "&safe_search=1&per_page=20&format=json&page=" + pageCount;
                String flickJSON = getJSON(flickrRequest,0);                
                flickJSON = flickJSON.substring(14, flickJSON.length() - 2); // We need to remove code at beginning and end of response
                FlickrResults flickResults = new Gson().fromJson(flickJSON,FlickrResults.class);
                
                if (flickResults.getImageResults().isEmpty()) {
                    updateMessage("Flickr.com -- No results found");
                } else {
                    updateMessage("Flickr.com -- Success!");
                    returnList = flickResults.getImageResults();
                }
                                
                return returnList;
                
            }
            
        };

        // The task for BigStock.com
        
        final Task<ArrayList<ImageResult>> bigStockTask = new Task<ArrayList<ImageResult>>() {

            ArrayList<ImageResult> returnList = new ArrayList<ImageResult>();
            
            @Override
            protected ArrayList<ImageResult> call() throws InterruptedException {

                updateMessage("Bigstock.com -- Checking ...");
                
                String bigStockRequest = "http://api.bigstockphoto.com/2/684259/search/?q=" + encodeSearch + "&page=" + pageCount + "&limit=20";
                String bigStockJSON = getJSON(bigStockRequest,0);
                BigStockResults bigResults = new Gson().fromJson(bigStockJSON, BigStockResults.class);       
                                
                if (bigResults.getImageResults().isEmpty()) {
                    updateMessage("Bigstock.com -- No results found");
                } else {
                    updateMessage("Bigstock.com -- Success!");
                    returnList = bigResults.getImageResults();
                }
                                
                return returnList;
                
            }
            
        };
                
        // The task for openclipart.org
                
        final Task<ArrayList<ImageResult>> clipArtTask = new Task<ArrayList<ImageResult>>() {

            ArrayList<ImageResult> returnList = new ArrayList<ImageResult>();
            
            @Override
            protected ArrayList<ImageResult> call() throws InterruptedException {
             
            updateMessage("Openclipart.org -- Checking ...");    
                
            String clipArtRequest = "http://openclipart.org/search/json/?query=" + encodeSearch + "&page=" + pageCount + "&amount=20";
            ClipArtResults clipResults = new Gson().fromJson(getJSON(clipArtRequest, 0), ClipArtResults.class);     
             
                if (clipResults.getImageResults().isEmpty()) {
                    updateMessage("Openclipart.org -- No results found");
                } else {
                    updateMessage("Openclipart.org-- Success!");
                    returnList = clipResults.getImageResults();
                }
                                
                return returnList;
                
            }
            
        };
        
        // Once we have created each of these three tasks, we then activate them
        // if their checkboxes are selected.  We also bind the checkbox text to each
        // task so we can update it once we're done downloading
        
        if(chkFlickr.isSelected()){
            
           chkFlickr.textProperty().bind(flickrTask.messageProperty());
           
           // If we're going to search the flickr repository, we set the
           // "setOnSucceeded" method for the flickrtask so that once the
           // task is succesfully completed, we create an ImageRetriever object
           // and pass to it the results of our search (in the form of a list 
           // of ImageResult objects which we retrieve using the flickrTask.get() method).
           
           // With that ImageRetriever created, we then call that object's launchDownload method
           // which in turn downloads the thumbnail images for the search results and adds
           // a new ThumbButton with the thumbnail image onto our tile pane.  See the 
           // code for ImageRetriever and ImagePlacer below for more details.
            
           // This pattern is used for all three image repositories
           
           flickrTask.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
           
               @Override
               public void handle(WorkerStateEvent t) {
                   chkFlickr.textProperty().unbind();
                   ImageRetriever newRetriever = null;
                                                       
                   try {
                       newRetriever = new ImageRetriever(flickrTask.get(), searchTerm);
                   } catch (InterruptedException ex) {
                       Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
                   } catch (ExecutionException ex) {
                       Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
                   }
                   
                   if(newRetriever != null){
                       newRetriever.launchDownload();
                   }
                   
                   
               }
            });
            
            executor.submit(flickrTask);
          
        }
                
        if(chkBigStock.isSelected()){
           
            chkBigStock.textProperty().bind(bigStockTask.messageProperty());
            bigStockTask.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
           
               @Override
               public void handle(WorkerStateEvent t) {
                   chkBigStock.textProperty().unbind();
                   
                   ImageRetriever newRetriever = null;
                   
                   try {
                       newRetriever = new ImageRetriever(bigStockTask.get(), searchTerm);
                   } catch (InterruptedException ex) {
                       Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
                   } catch (ExecutionException ex) {
                       Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
                   }
                   
                   if(newRetriever != null){
                       newRetriever.launchDownload();
                   }
                   
               }
            });
            
            executor.submit(bigStockTask);
        
        }
        
        if(chkClipArt.isSelected()){
            
           chkClipArt.textProperty().bind(clipArtTask.messageProperty());
           clipArtTask.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
           
               @Override
               public void handle(WorkerStateEvent t) {
                   chkClipArt.textProperty().unbind();

                   ImageRetriever newRetriever = null;
                   
                   try {
                       newRetriever = new ImageRetriever(clipArtTask.get(), searchTerm);
                   } catch (InterruptedException ex) {
                       Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
                   } catch (ExecutionException ex) {
                       Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
                   }
                   
                   if(newRetriever != null){
                       newRetriever.launchDownload();
                   }
                   
                   
               }
            });
            
            executor.submit(clipArtTask);
            
      
        }

        // </editor-fold>
               
        executor.shutdown();
        
        // We adjust the search button to allow for loading additional pages
        
        btnSearch.setText("Load Page " + (pageCount + 1));
        btnSearch.setDisable(false);
        btnDownloadAll.setDisable(false);
        newSearch = false;
        
    }
     
    // <editor-fold defaultstate="collapsed" desc="getJSON">
    
    /* This code is taken directly from the instructor's NytController class */
    
    public String getJSON(String url, int timeout) {
    
        try {
            
            URL u = new URL(url);
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setRequestProperty("Content-length", "0");
            c.setUseCaches(false);
            c.setAllowUserInteraction(false);
            c.setConnectTimeout(timeout);
            c.setReadTimeout(timeout);
            c.connect();
            int status = c.getResponseCode();

            switch (status) {
                case 200:
                case 201:
                    BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line+"\n");
                    }
                    br.close();
                    return sb.toString();
            }

        } catch (MalformedURLException ex) {
           // Logger.getLogger(DebugServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
           // Logger.getLogger(DebugServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="ImageRetriever and ImagePlacer">
    
    // This class is used to retrieve thumbnails based upon a list of ImageResults
    // as well as a search term. It does this by creating an ImagePlacer object 
    // (shown below) which takes an ImageResult, downloads the thumbnail image
    // and builds a ThumbButton button from it.
    
    // We append the search term used to generate the search to each image so that we
    // can know which folder in which to save an image.
      
    private class ImageRetriever {

        private final ArrayList<ImageResult> imageList;
        private ThreadPoolExecutor executor;
        private String searchTerm;

        public ImageRetriever(ArrayList<ImageResult> targetList, String searchTerm){
            this.imageList = targetList;
            this.searchTerm = searchTerm;
        }

        // This code was inspired by the code example found at:
        // http://www.tutorialspoint.com/java_dip/downloading_uploading_images.htm

        public void launchDownload(){

            executor = (ThreadPoolExecutor)Executors.newFixedThreadPool(5);

            for(int i = 0; i < imageList.size(); i++){
                
                imageList.get(i).setSearchTerm(searchTerm);
                ImagePlacer tempPlacer = new ImagePlacer(imageList.get(i));
                executor.submit(tempPlacer);
            }

            executor.shutdown();


        }

        // After the thumnbail is downloaded as a stream (not a file) and attached to the ImageResult,
        // it is passed to the ImagePlacer which creates a new ThumbButton
        // which is placed onto the middle tile pane.
        
        private class ImagePlacer implements Runnable {

                private ImageResult imageResult;
                protected ThumbButton tempButton;

                public ImagePlacer(ImageResult targetResult){
                     this.imageResult = targetResult;
                }

                @Override
                public void run() {

                    URL url = null;

                    try {
                        url = new URL(imageResult.getThumbURL());
                    } catch (MalformedURLException ex) {
                        Logger.getLogger(ImageRetriever.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    InputStream inputStream = null;

                    try {
                        inputStream = url.openStream();
                    } catch (IOException ex) {
                        Logger.getLogger(ImageRetriever.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    Image tempImage = new Image(inputStream,75,75,false,true);
                    ImageView tempIE = new ImageView(tempImage);
                    tempIE.setFitWidth(75);
                    tempIE.setPreserveRatio(true);
                    tempIE.setSmooth(true);

                    tempButton = new ThumbButton();
                    tempButton.getStyleClass().add("tempButton");
                    tempButton.setGraphic(new ImageView(tempImage));
                    tempButton.setFullURL(imageResult.getFullURL());
                    tempButton.setPrefSize(100, 100);
                    
                    tempButton.setOnAction(new EventHandler<ActionEvent>() {
                        @Override public void handle(ActionEvent e) {
                            
                            try {
                                downloadFile(imageResult); // This will download the full image file
                                tempButton.setDisable(true);
                            } catch (IOException ex) {
                                Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    });

                    Platform.runLater(new Runnable() {
                        @Override public void run() {
                            tileMiddle.getChildren().add(tempButton);
                        }
                    });

                }

            }
    
        }
    
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Select Directory">
    
    // This code allows the user to select a new directory in which to save images
    
    @FXML
    void selectDirectory(ActionEvent event) {

        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Select Root Directory");

        File file = directoryChooser.showDialog(null);

        if (file != null) {
            txtSelectDir.setText(file.getAbsolutePath());
            fileDirectory = file.getAbsolutePath();
        }

    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Download Full File">
        
    // This code receives an ImageResult (one image from a repository) and attempts to download
    // the full-sized image associated with that ImageResult to designated folder on the local computer.
    
    public void downloadFile(ImageResult targetResult) throws MalformedURLException, IOException{
        
        final ImageResult endResult = targetResult;
        
        // Make sure that we have a target directory selected (created)
        
        if(txtSelectDir.getText().trim().equals("")){
           Action response = Dialogs.create()
           .title("No Root Directory Selected")
           .masthead("You must select a root directory")
           .message("Before downloading any images, you must select a root directory in which to store them.")
           .showError();
           return;
        }
        
        // see if target directory exists; if not, create it.
     
        String targetSubDir = targetResult.getSearchTerm().replaceAll(" ", "_").toLowerCase();
        final String totalDir;
        
        // Check to see if target directory already has '\' attached to it
        
        if(fileDirectory.substring(fileDirectory.length()-1, fileDirectory.length()).equals("\\")){
           totalDir = fileDirectory + targetSubDir;
        } else {
           totalDir = fileDirectory + "//" + targetSubDir;
        }
        
        File newDirectory = new File(totalDir);
        
        if(!newDirectory.exists()){
            
            try{
                newDirectory.mkdir();
            } catch (Exception e) {
                Action response = Dialogs.create()
                  .title("Unable To Create Root Directory")
                  .masthead("You must select a valid root directory")
                  .message("Before downloading any images, you must select a valid root directory in which to store them.")
                  .showError();
                  return;
            }
        } 
        
      // Now create target file name and check if it already exists
        
        final String targetFile = totalDir + "//" + targetResult.getLocalFileName();
        File testFile = new File(targetFile);

        // Enable the clearDownloads button
        
        Platform.runLater(new Runnable() {
            public void run() {
               btnClearDownloads.setDisable(false);
               btnDownloadAll.setDisable(false);
            }
        });
        
        
        // Here we set up the executor for download the thumbnail
              
        ExecutorService executor = (ExecutorService)Executors.newSingleThreadExecutor();        
        FileDownload newDownload = new FileDownload(targetResult.getFullURL(),targetFile);
        final Future<?> downloadFuture = executor.submit(newDownload);

        // This is where we build an entry for our download 
        // in the bottom panel. This entry consists of a horizontal box with a label
        // showing the file name, a progress bar, and three buttons: "Delete" (to delete the file), 
        // "show" to show the file and "Folder" to open the folder where the image is stored.
        
        newHBox = new HBox(15);
        final HBox tempBox = newHBox;
        
        tempBox.setPrefSize(350, 35);
        tempBox.setPadding(new Insets(5));
        tempBox.setAlignment(Pos.CENTER_LEFT);
        tempBox.getStyleClass().add("hbox");
                
        newProgBar = new ProgressBar();
        final ProgressBar tempProgBar = newProgBar;
        tempProgBar.setPrefWidth(300);
                
        String imageDescriptive = String.format("%-30s",targetResult.getLocalFileName());

        lblDescription = new Label(imageDescriptive);
        final Label tempDescription = lblDescription;
        
        tempDescription.setFont(new Font( "Monospaced", 10 ));

        // Note that we use "temp" buttons to point to the original button
        // which is "over-written" when we download a new file.  This is done in this
        // manner because we need to identify each button on our main thread.
        
        btnShow = new Button("Show");
        final Button tempShow = btnShow;
        
        btnFolder = new Button("Folder");
        final Button tempFolder = btnFolder;
        
        btnCancel = new Button("Delete");      
        final Button tempCancel = btnCancel;
        
        // When the user presses the button 'Delete' button for a particular download
        
        tempCancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {

                if(downloadFuture.isDone()){
                    
                    Action response = Dialogs.create()
                            .title("Delete Image?")
                            .message("Are you sure you want to delete this image?")
                            .showConfirm();

                    if(response != Dialog.Actions.YES){
                        return;
                    }
                    
                    File fileName = new File(targetFile);
                    
                    if(fileName.exists()){                    
                       fileName.delete();
                    } else {
                        Action badFileName = Dialogs.create()
                                .title("Unable To Delete Image")
                                .message("We were unable to delete this image.  This may be because another process altered or removed this file already. Check the folder and try again.")
                                .showError();
                        return;
                    }
                
                } else {
                    downloadFuture.cancel(true);
                }

                Platform.runLater(new Runnable() {
                    public void run() {
                        tempShow.setDisable(true);
                        tempCancel.setDisable(true);
                    }
                });
                
            }
        });
        
        // When the user presses the button 'Show' button for a particular download
        
        tempShow.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                File fileName = new File(targetFile);             
                Desktop desktop = Desktop.getDesktop();
                
                if(fileName.exists() == false){
                   Action response = Dialogs.create()
                       .title("Image Unavailable")
                       .message("This image is unavailable for viewing. Please check the folder and try again.")
                       .showWarning();
                   return;
                }
                
                try {
                    desktop.open(fileName);
                } catch (IOException ex) {
                   Action response = Dialogs.create()
                       .title("Unavailable To View File")
                       .message("We are unable to display this file for viewing. This may be due to a problem with your system configuration or the manner in which the image was saved. Please check the image file in its folder and try again.")
                       .showWarning();
                   return;
                }
            }
        });       
        
        // When the user presses the button 'Folder' button for a particular download
        
        tempFolder.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                File fileDir = new File(totalDir);             
                Desktop desktop = Desktop.getDesktop();
                
                if(fileDir.exists() == false){
                   Action response = Dialogs.create()
                       .title("Directory Unavailable")
                       .message("This directory is unavailable for viewing. It may have been deleted manually or there may be some other problem with it.  Please check your system and try again.")
                       .showError();
                   return;
                }
                
                try {
                    desktop.open(fileDir);
                } catch (IOException ex) {
                    Logger.getLogger(MainScreenController.class.getName()).log(Level.SEVERE, null, ex);
                    Action response = Dialogs.create()
                       .title("Unavailable To View Folder")
                       .message("This directory is unavailable for viewing. It may have been deleted manually or there may be some other problem with it.  Please check your system and try again.")
                       .showError();
                   return;

                }
            }
        });
        
        Platform.runLater(new Runnable() {
           @Override public void run() {
             tempBox.getChildren().addAll(tempDescription, tempProgBar, tempCancel, tempShow, tempFolder);
             vbox.getChildren().add(tempBox);
             scrollBottom.setVvalue(2.0); 
           }
        });
                
        tempProgBar.progressProperty().bind(newDownload.progressProperty());
        executor.shutdown();
                  
    }
    
    // This class receives a URL and a local file name and attempts to download
    // the remote file to the local file.
    
    class FileDownload extends Task<Void>{

        private String targetFile;
        private String localFile;
                
        public FileDownload(String targetFile, String localFile){
            this.targetFile = targetFile;
            this.localFile = localFile;
        }
        
        @Override
        protected Void call() throws Exception {
                              
            URL url = new URL(targetFile);
            URLConnection connection = url.openConnection();

            
            int fileLength = connection.getContentLength();
            int currentLength = 0;

            OutputStream os = null;
            this.updateProgress(0.0,fileLength);
            
            try (InputStream is = url.openStream()) {
                
                try {
                    os = new FileOutputStream(localFile);
                } catch (FileNotFoundException e) {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            Action response = Dialogs.create()
                                    .title("Unable To Save Local File")
                                    .message("We were unable to save this image to your machine.  Please check the root directory and try again.")
                                    .showError();
                          }
                    });
                    os.close();
                    return null;
                }
                
                byte[] b = new byte[2048];
                int length;
                while ((length = is.read(b)) != -1) {
                    os.write(b, 0, length);
                    currentLength += length;
                    this.updateProgress(currentLength, fileLength);
                }
            }

            os.close(); 
            return null; 
 
        }
                
    }
    
    // </editor-fold>

    @FXML
    private void clearDownloads(javafx.event.ActionEvent event){
        
        // We simply clear the vbox and reset the scroll bar to go to the top
        
        vbox.getChildren().clear();
        scrollBottom.setVvalue(0.0);
        Platform.runLater(new Runnable() {
            public void run() {
                btnClearDownloads.setDisable(true);
            }
        });;
    }
       
   @FXML
    private void downloadAll(javafx.event.ActionEvent event){
      
       // We simply go through each button in the tileMiddle tile pain
       // and press them.  That's it.
        
       for(Node tempNode : tileMiddle.getChildren() ){
           if(tempNode instanceof ThumbButton){
               ThumbButton tempButton = (ThumbButton) tempNode;
               tempButton.fire();
           }
       }
      
       Platform.runLater(new Runnable() {
          public void run() {
               btnDownloadAll.setDisable(true);
         }
       });;
      
    }
    
}
