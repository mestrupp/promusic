
/*
 * This class merely extends the normal JavaFX button and allows us
 * to attach a URL that points to the full-sized image referenced 
 * by the thumbnail image displayed on the button.
 */

package edu.uchicago.mestrupp;

import javafx.scene.control.Button;

public class ThumbButton extends Button {
    
    private String fullURL;

    public ThumbButton(){
        super();
    }
    
    public String getFullURL() {
        return fullURL;
    }

    public void setFullURL(String fullURL) {
        this.fullURL = fullURL;
    }
        
}
