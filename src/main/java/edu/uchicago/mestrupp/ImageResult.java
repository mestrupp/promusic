
/* The ImageResult class is used to store information about images stored
 * in the various repositories we are searching.  This information is gleaned
 * from the search results from the various repositories, and includes information
 * about where to find the images (including thumbnail images).  The ImageResult
 * also stores the search term(s) that generated the results through which
 * it was created.
 */

package edu.uchicago.mestrupp;

import javafx.scene.image.Image;

public class ImageResult {

    private String source;
    private Image thumbImage = null;
    
    private String thumbURL;
    private String fullURL;
    private String localFileName;
    private String id;
    
    private String searchTerm;

    public ImageResult(String id, String source, String thumbURL, String fullURL) {
        
        this.source = source;
        this.thumbURL = thumbURL;
        this.fullURL = fullURL;
        this.id = id;
        
        localFileName = this.source + "_" + this.id + ".png";
     
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

        public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }
        
    
    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
    
    public Image getThumbImage() {
        return thumbImage;
    }

    public void setThumbImage(Image thumbImage) {
        this.thumbImage = thumbImage;
    }

    public String getThumbURL() {
        return thumbURL;
    }

    public void setThumbURL(String thumbURL) {
        this.thumbURL = thumbURL;
    }

    public String getFullURL() {
        return fullURL;
    }

    public void setFullURL(String fullURL) {
        this.fullURL = fullURL;
    }
    
    public String getLocalFileName(){
        return localFileName;
    }
    
    
}
