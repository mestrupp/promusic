

/* This class serves as a "catch" for the GSON-parsing of search data from openclipart.org */

package edu.uchicago.mestrupp;

import java.util.ArrayList;

public class ClipArtResults {
    
    public String msg;
    public Info info;
    public Integer num_results;
    
    public Payload[] payload;
    
    private class Info {
        
        public Integer results;
        public Integer pages;
        public Integer current_page; 
    }
    
    private class Payload {
        
        public String title;
        public Integer total_favorites;
        public String created;
        public String id;
        
        public Svg svg;
        public Dimensions dimensions;
        
        private class Svg {
                       
            public String url;
            public String png_thumb;
            public String png_full_lossy;
            public String png_2400px;
        }
        
        private class Dimensions {
            
            public Png_thumb png_thumb;
            public Png_full_lossy png_full_lossy;
            
            private class Png_thumb {
                public Integer width;
                public Integer height;
            }
            
            private class Png_full_lossy {
                public Integer width;
                public Integer height;
            }
        }
        
               
    }
    
    @Override
    public String toString(){
        
        String responseString = "";
        
        for (Payload sample : payload){
            responseString += sample.title + " Thumbnail URL: " + sample.svg.png_thumb.toString() + "\n";
        }
        
        return responseString;
     }
    
     // Returns a list of ImageResult objects created based upon the parsed JSON data
    
     public ArrayList<ImageResult> getImageResults(){

         ArrayList<ImageResult> resultList = new ArrayList<>();
         
         for (Payload sample : payload){
             ImageResult tempIR = new ImageResult(sample.id, "openclipart",sample.svg.png_thumb, sample.svg.png_2400px);
             resultList.add(tempIR);
         }
         
         return resultList;
         
     }
    
}
