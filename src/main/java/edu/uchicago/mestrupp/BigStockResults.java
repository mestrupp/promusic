
/* This class serves as a "catch" for the GSON-parsing of search data from Bigstock.com */

package edu.uchicago.mestrupp;

import java.util.ArrayList;

public class BigStockResults {

    public Integer response_code;
    public String message;
    
    public Data data;
    
    private class Data {
        
        public Paging paging;
        public Images[] images;
                
        private class Paging {
            
            public Integer items;
            public Integer items_per_page;
            public Integer total_items;
            public Integer page;
            public Integer total_pages;
            
        }
        
                
    }
    
    protected class Images {
            
            public String id;
            public String title;
            
            public Small_Thumb small_thumb;
            
            private class Small_Thumb {
                
                public String url;
                public Integer width;
                public Integer height;
                                
            }
                        
    }
    
    @Override
    public String toString(){
        
        String responseString = "";
        
        for (Images sample : data.images){
            responseString += sample.title + " Thumbnail URL: " + sample.small_thumb.url.toString() + "\n";
        }
        
        return responseString;
        
    }
    
    // Returns a set of ImageResult objects based upon the parsed data
    
    public ArrayList<ImageResult> getImageResults(){

         ArrayList<ImageResult> resultList = new ArrayList<>();
         
         for (Images sample : data.images){
             ImageResult tempIR = new ImageResult(sample.id, "bigstock",sample.small_thumb.url,sample.small_thumb.url);
             resultList.add(tempIR);
         }
         
         return resultList;
         
     }
    
}
