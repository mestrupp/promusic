

/* This class serves as a "catch" for the GSON-parsing of search data from flickr.com */

package edu.uchicago.mestrupp;

import java.util.ArrayList;

public class FlickrResults {
    
    public String stat;
    public Photos photos;
    
    private class Photos {
        
        public Integer page;
        public Integer pages;
        public Integer perpage;
        public String total;
        
        Photo[] photo;
      
    }

    protected class Photo {
            
        public String id;
        public String owner;
        public String secret;
        public String server;
        public Integer farm;
        public String title;
        public Integer ispublic;

        public String getImageURL(String imageType) {
            if(imageType.equals("")){
                 return "https://farm" + farm.toString() + ".staticflickr.com/" + server + "/" + id + "_" + secret + ".jpg";
            }
            return "https://farm" + farm.toString() + ".staticflickr.com/" + server + "/" + id + "_" + secret + "_" + imageType + ".jpg";
        }
        
    }
    
    @Override
    public String toString(){
        
        String responseString = "";
        
        for (Photo sample : photos.photo){
            responseString += sample.title + "\n";
        }
        
        return responseString;
        
    }
    
    
    // This returns a list of ImageResult objects based upon the parsing of search results
    
    public ArrayList<ImageResult> getImageResults(){

         ArrayList<ImageResult> resultList = new ArrayList<>();
         
         for (Photo sample : photos.photo){
             ImageResult tempIR = new ImageResult(sample.id, "flickr",sample.getImageURL("t"),sample.getImageURL(""));
             resultList.add(tempIR);
         }
         
         return resultList;
         
     }
    
    
    
}
